import React, { Component } from 'react';
import ReactDOM from "react-dom";
//import FormComponent from './components/FormComponent'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//import Home from "./app";
import Home from "./components/HomeComponent";

import Navbar from "./components/NavbarComponent";

import News from "./components/NewsComponent";

import Department from "./components/DepartmentComponent";

import Gallery from "./components/GalleryComponent";

import Contact from "./components/ContactComponent";

import './App.css';
import "./styles.css";
function App() {
  return (
    <div>
     {/* <FormComponent/> */}
     <Router>
      <div className="App">
        <Navbar />
        <div>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/news" component={News} />
            <Route path="/departments" component={Department} />
            <Route path="/gallery" component={Gallery} />
            <Route path="/contact" component={Contact} />
          </Switch>
        </div>
      </div>
    </Router>
    </div>
  );
}

export default App;
